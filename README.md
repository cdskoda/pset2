
         P R E L I M I N A R Y    S P E C I F I C A T I O N

      Due 2:00 AM, Friday, 29 September 2017

CPSC 323   Homework #2   Parsing (Some) Bash Commands

REMINDER:  Do not under any circumstances copy another student's code or give
a copy of your code to another student.  After discussing the assignment with
another student, do not take any written or electronic record away and engage
in a full hour of mind-numbing activity before you work on it again.  Such
discussions should be noted in your log file.

Sharing with another student ANY written or electronic document (e.g., code
or test cases) related to the course is a violation of this policy.


(50 points) Write a command line parser "Parse" that prompts for and reads
bash-like command lines from stdin, breaks the lines into tokens, parses the
tokens into a tree of commands, and writes the tree in in-order to stdout.
Here a token is one of the following.

(1) a maximal, contiguous, nonempty sequence of nonwhitespace characters
    other than the metacharacters <, >, ;, &, |, (, and ) [a SIMPLE token];

(2) a redirection symbol (<, <<, >, >>, 2>, 2>>, or &>);

(3) a pipeline symbol (|);

(4) a command operator (&& or ||);

(5) a command terminator (; or &);

(6) a left or right parenthesis (used to group commands).

Example:  The command line

    < A B | ( C 2> D & E < F ) > G ; H=I J K

  results in the following command tree

  .            ;
  .          /   \
  .      PIPE     H=I J K
  .     /    \
  .   B <A    SUB >G
  .          /
  .         &
  .        / \
  .   C 2>D   E <F

  which is printed as

    CMD (Depth = 2):  SIMPLE,  argv[0] = B  <A
    CMD (Depth = 1):  PIPE
    CMD (Depth = 4):  SIMPLE,  argv[0] = C  2>D
    CMD (Depth = 3):  SEP_BG
    CMD (Depth = 4):  SIMPLE,  argv[0] = E  <F
    CMD (Depth = 2):  SUBCMD  >G
    CMD (Depth = 0):  SEP_END
    CMD (Depth = 1):  SIMPLE,  argv[0] = J,  argv[1] = K
       LOCAL: H=I,

The file Hwk2/mainParse.c contains the main program for Parse; your task is to
write the function parse(), which parses the linked list of tokens returned by
Hwk2/tokenize.o and creates a tree of command structs as specified in
Hwk2/parse.h.

The function tokenize() handles the following bash-like features for you:

* [Comments] If a # would begin a SIMPLE token, it and the remainder of the
  line (including any trailing \'s) are ignored; otherwise it is just another
  character.  A leading # can be escaped.

* [Escape Characters] The escape character \ removes any special meaning that
  is associated with the following non-null, non-newline character.  This can
  be used to include whitespace (but not newlines), metacharacters, and quotes
  in a SIMPLE token.  The escape character is removed.

* [Single Quoted Strings] All characters appearing between matching single
  quotes (') are treated as if they were nonwhitespace, non-metacharacters.
  Such strings may not contain single quotes, even when preceded by a \.  The
  surrounding quotes are removed.  If there is no matching quote, tokenize()
  writes an error message and returns NULL.

* [Double Quoted Strings] All characters appearing between matching double
  quotes (") are treated as if they were nonwhitespace, non-metacharacters,
  except that a \ followed by a " or \ acts as an escape character and removes
  any special meeting associated with the " or \.  The surrounding quotes and
  any escape characters are removed.  If there is no matching quote, tokenize()
  writes an error message and returns NULL.

While creating the command tree, parse() must also handle the following
bash-like features:

* [Local Variables] The assignment of a value to an environment variable
  when the command executes can be specified by a SIMPLE token of
  the form NAME=VALUE, where NAME is a sequence of one or more alphanumeric
  and underscore characters that does not begin with a digit and VALUE may
  be empty.  Such assignments must precede the zero-th argument of a simple
  command or the opening left parenthesis of a subcommand.

* [HERE Documents] The redirection <<WORD specifies a HERE document, where
  WORD may be any SIMPLE token and there can be whitespace between << and
  WORD.  Subsequent lines up to and not including a line containing only
  WORD (or an EOF) are concatenated together and redirected to the standard
  input of the command.  If the last line does not contain a newline,
  one is appended.

  The lines in a HERE document are read from the standard input when the
  <<WORD redirection is parsed.

  After the entire simple command or subcommand with which it is associated
  has been parsed, the HERE document undergoes further processing:

  + Environment variables are expanded.  That is, each sequence $NAME is
    replaced by the value of the environment variable NAME (or the empty string
        if NAME is undefined), where NAME is a maximal sequence of one or more
    alphanumeric or _ characters that does not begin with a digit.

  + [Optional] Any \ followed by a newline is treated as a line splice and
    deleted.

  + A \ that escapes a following \ or $ is deleted.

Use the submit command to turn in your log file (see Homework #1) and the C
source file(s) for Parse (including a Makefile but NOT parse.h or mainParse.c).

YOU MUST SUBMIT YOUR FILES (INCLUDING THE LOG FILE) AT THE END OF ANY SESSION
WHERE YOU HAVE SPENT AT LEAST ONE HOUR WRITING OR DEBUGGING CODE, AND AT LEAST
ONCE EVERY HOUR DURING LONGER SESSIONS.  (All submissions are retained.)


Notes
~~~~~
1. Read and understand the Appendix and Hwk2/parse.h BEFORE you write any code!

2. See Matthew and Stones, Chapter 2, for a general description of bash.  See
   "man bash" or "info bash" for a complete description of local variables,
   the various redirection operators (<, <<, >, >>, 2>, 2>>, &>), and HERE
   documents.  But bear in mind that there are many features that Parse does
   not implement.

   Although they are implemented in the reference solution, your parse()
   NEED NOT handle redirection involving stderr, that is, 2>, 2>>, and &>.
   They will not be tested.

3. DO NOT MODIFY Hwk2/parse.h or Hwk2/mainParse.c or Hwk2/tokenize.o -- the
   source code for parse() should be in a different file (or files).  To
   enforce this the test script may delete/rename local files with the names
   parse.h, mainParse.c, and tokenize.o.

4. To avoid a limit on the size of a line, mainParse uses the function
   getline().  (See "man 3 getline".)

5. Parse should detect errors such as:

   * unbalanced parentheses; e.g.,
       (ls bar ; ls foo

   * a missing name following a redirection symbol; e.g.,
       ls >

   * multiple input, output, or error redirection; e.g.,
       ls >bar | wc

   * both a command and a subcommand or two subcommands; e.g.,
       ls (cat /etc/motd)

   * a missing command; e.g.,
       ls | | wc

   writing a one-line error message to stderr and returning NULL (which denotes
   the empty command).
   WARNING: A few of the public tests involve error checking, but parse() will
   need to do a LOT more to pass all of the final tests.

6. The behavior of Parse does not match bash in all particulars, including:

   a. Parse does not recognize line splices outside of HERE documents.

   b. Within a double-quoted string Parse does not treat a \ followed by a $,
      `, or ! as an escape character.

   c. Parse allows local variable definitions and input, output, or error
      redirection _before_ a subcommand.

   d. Commands like
  > FILE
      (missing command) and
  command >file1 >>file2
      (multiple input, output, or error redirection) generate error messages.

   e. Parse does not issue a prompt for each line in a HERE document when run
      interactively.

   In such instances, the behavior of Hwk2/Parse is what your code should
   produce.  Note: A list of other known differences will be maintained at
   Hwk2/Differences.  Please report any others that you discover.

7. mainParse.c contains functions that you may find useful for implementing and
   debugging Parse:  mallocCMD() and freeCMD() to create and free an empty CMD
   struct; dumpList() to print a token list; and dumpTree() to print a command
   tree.

8. tokenize() will not return a token whose type is NONE or ERROR.  Thus
   parse() may use NONE and ERROR as you see fit, but note that mallocCMD()
   uses NONE as the default value for fromType, toType, and errType.
9. For simplicity, you may ignore the possibility of error returns from
   malloc()/realloc().  However, you must free() all such storage when it is no
   longer needed, so that all storage has been freed when the program exits.

A. Use the library function getenv() to fetch the value of an environment
   variable.  (See "man 3 getenv" for details.)

B. The further processing of a HERE document should run in time at most
   quadratic in the sum of the lengths of the initial document and the values
   of any variables that are expanded.

Appendix
~~~~~~~~
   The syntax for a command is

     <local>    = VARIABLE=VALUE
     <red_op>   = < / << / > / >> / 2> / 2>> / &>
     <redirect> = <red_op> FILENAME
     <prefix>   = <local> / <redirect> / <prefix> <local> / <prefix> <redirect>
     <suffix>   = SIMPLE / <redirect> / <suffix> SIMPLE / <suffix> <redirect>
     <redList>  = <redirect> / <redList> <redirect>
     <simple>   = SIMPLE / <prefix> SIMPLE / SIMPLE <suffix>
                         / <prefix> SIMPLE <suffix>
     <subcmd>   = (<command>) / <prefix> (<command>) / (<command>) <redList>
                              / <prefix> (<command>) <redList>
     <stage>    = <simple> / <subcmd>
     <pipeline> = <stage> / <pipeline> | <stage>
     <and-or>   = <pipeline> / <and-or> && <pipeline> / <and-or> || <pipeline>
     <sequence> = <and-or> / <sequence> ; <and-or> / <sequence> & <and-or>
     <command>  = <sequence> / <sequence> ; / <sequence> &

   Note that FILENAME = SIMPLE.

   A command is represented as a tree of CMD structs containing its <simple>
   commands and the "operators" | (= PIPE), && (= SEP_AND), || (= SEP_OR),
   ; (= SEP_END), & (= SEP_BG), and SUBCMD.  The command tree is determined
   by (but is not equal to) the parse tree in the above grammar.

   The tree for a <simple> is a single struct of type SIMPLE that specifies its
   arguments (argc, argv[]); its local variables (nLocal, locVar[], locVal[]);
   and whether and where to redirect its standard input (fromType, fromFile),
   its standard output (toType, toFile), and its standard error (errType,
   errFile).  The left and right children are NULL.

   The tree for a <stage> is either the tree for a <simple> or a CMD struct of
   type SUBCMD (which may have local variables and redirection) whose left
   child is the tree representing a <command> and whose right child is NULL.
   Note that I/O redirection is associated with a <stage> (i.e., a <simple> or
   <subcmd>), but not with a <pipeline> (redirection for the first/last stage
   is associated with the stage, not the pipeline).

   The tree for a <pipeline> is either the tree for a <stage> or a CMD struct
   of type PIPE whose right child is a tree representing the last <stage> and
   whose left child is the tree representing the rest of the <pipeline>.
   The tree for an <and-or> is either the tree for a <pipeline> or a CMD
   struct of type && (= SEP_AND) or || (= SEP_OR) whose left child is a tree
   representing an <and-or> and whose right child is a tree representing a
   <pipe-line>.

   The tree for a <sequence> is either the tree for an <and-or> or a CMD
   struct of type ; (= SEP_END) or & (= SEP_BG) whose left child is a tree
   representing a <sequence> and whose right child is a tree representing an
   <and-or>.

   The tree for a <command> is either the tree for a <sequence> or a CMD
   struct of type ; (= SEP_END) or & (= SEP_BG) whose left child is the tree
   representing a <sequence> and whose right child is NULL.

   Examples (where A, B, C, D, and E are <simple>):
   
                                Command Tree

     < A B | C | D | E > F                     PIPE
                                              /    \
                                          PIPE      E >F
                                         /    \
                                     PIPE      D
                                    /    \
                                <A B      C

     A && B || C && D                   &&
                                       /  \
                                     ||    D
                                    /  \
                                  &&    C
                                 /  \
                                A    B

     A ; B & C ; D || E ;                 ;
                                        /
                                       ;
                                     /   \
                                    &     ||
                                   / \   /  \
                                  ;   C D    E
                                 / \
                                A   B

     (A ; B &) | (C || D) && E                 &&
                                              /  \
                                          PIPE    E
                                         /    \
                                      SUB      SUB
                                     /        /
                                    &       ||
                                   /       /  \
                                  ;       C    D
                                 / \
                                A   B

                CS-323-09/15/17
