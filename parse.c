#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "/c/cs480/Hwk2/parse.h"

CMD *commandTree(token *tok) ;
CMD *sequenceTree(token *tok) ;
CMD *andorTree(token *tok) ;
CMD *pipelineTree(token *tok) ;
CMD *stageTree(token *tok) ;
CMD *subcommandTree(token *tok, CMD *prefix) ;
CMD *simpleTree(token *tok) ;

CMD *parse(token *tok){
	// SimpleTree Test
	CMD *cmd = simpleTree(tok) ;
	// go through cmd struct to do parse() functions outside of make tree
	// todo:
	// [Local Variables] function
	// [HERE Documents] function
	return cmd ;
}//end parse/main function


// Given a string, determines if is a local
bool isLocal(char *text){
    // Check first character for alpha or _
    if(!isalpha(text[0]) && text[0] != '_')
        return false ;
    // Look for only alphanumerics until = found
    int i = 1 ;
    while(i < strlen(text)){
        // Check if '=' found
        if(text[i] == '=')
            return true ;
        // Check if is alphanumeric or _
        if(!isalpha(text[i]) && !isdigit(text[i]) && text[i] != '=')
            return false ;
        i++ ;
    }//end while going through text
    return false ;
}//end isLocal

// returns <simple> tree
CMD *simpleTree(token *tok){
	// check if passed NULL token
	if(tok == NULL)
		return NULL ;
	CMD *simple = mallocCMD() ;
	simple->type = SIMPLE ;
	bool simple_flag = false ;
	while(RED_OP(tok->type) || tok->type == SIMPLE){
		// Check if is local
		if(isLocal(tok->text)){
			// Check if local listed after arg
			if(simple_flag){
				freeCMD(simple) ;
				return NULL ;
			}//end if simple/arg listed before local
			char *local ;
			local = strtok(tok->text, "=") ;
			simple->locVar[simple->nLocal] = local ;
			local = strtok(NULL, "") ;
			simple->locVal[simple->nLocal] = local ;
			simple->nLocal++ ;
			tok = tok->next ;
		}//end if is local
		// Check if is RED_OP/Here Doc
		else if(RED_OP(tok->type)){
			// Check for SIMPLE after RED_OP
			if(tok->next && tok->next->type == SIMPLE){
				// fromType
				if(tok->type == RED_IN || tok->type == RED_IN_HERE){
					simple->fromType = tok->type ;
					tok = tok->next ;
					simple->fromFile = tok->text ;
					tok = tok->next ;
				}//end if fromType
				// toType
				if(tok->type == RED_OUT || tok->type == RED_OUT_APP || tok->type == RED_OUT_ERR){
					simple->toType = tok->type ;
					tok = tok->next ;
					simple->toFile = tok->text ;
					tok = tok->next ;
				}//end if toType
				// errType
				if(tok->type == RED_ERR || tok->type == RED_ERR_APP || tok->type == RED_OUT_ERR){
					simple->errType = tok->type ;
					tok = tok->next ;
					simple->errFile = tok->text ;
					tok = tok->next ;
				}//end if errType
			}//end check
			else{
				free(simple) ;
				return NULL ;
			}//end if no SIMPLE listed after RED_OP
		}//end if redirect
		// Else is Simple
		else if(tok->type == SIMPLE){
			simple_flag = true ;
			simple->argv[simple->argc] = malloc(sizeof(char) * (strlen(tok->text) +1)) ;
			strcpy(simple->argv[simple->argc], tok->text) ;
			simple->argc++ ;
			tok = tok->next ;
		}//end else is arg/SIMPLE
		// <prefix>(<subcommand>) case
		else if(tok->type == PAR_LEFT){
			// check for SIMPLES/args/command before subcommand
			tok = tok->next ;
			if(simple_flag)
				return NULL ;
			simple->type = SUBCMD ;
			simple = subcommandTree(tok, simple) ;
			return simple ;
		}//end if subcommand present
	}//end while getting redirects, args, or locals
	return simple ;
}//end simpleTree

CMD *subcommandTree(token *tok, CMD *prefix){
	// Save pointer to <command> part
	token *commandstart = tok ;
	int parcount = 1 ;
	// need to figure out how to deal with <prefix> possibly before, recursive <subcommand>, and possible <redList> after
	// continue until we reach null or paranthesis balance out
	// check for next to tok so end on final PAR_LEFT and able to seperate into <command> and <redList>
	while(tok != NULL || parcount == 0){
		if(tok->type == PAR_LEFT)
			parcount++ ;
		if(tok->type == PAR_RIGHT)
			parcount-- ;
		tok = tok->next ;
	}////end while get <subcommand>
	// check if able to balance out paranthesis
	if(tok == NULL)
		return NULL ;
	// now at last par_right token, seperate into <command> and <redList>
	free(commandstart) ;
	return NULL ;
}//end subcommandTree

// returns <stage> tree
CMD *stageTree(token *tok){
	// get <simple> and check for success
	CMD *simple = simpleTree(tok) ;
	if(simple == NULL)
		return NULL ;
	// case where <stage> = <simple>
	return simple ;
}//end stageTree

// returns <pipeline> tree
CMD *pipelineTree(token *tok){
	// get <stage> and check for success
	CMD *stage = stageTree(tok) ;
	if(stage == NULL)
		return NULL ;
	// case where <pipeline>|<stage>
	if(tok->type == PIPE){
		CMD *pipeline = mallocCMD() ;
		pipeline->type = tok->type ;
		tok = tok->next ;
		pipeline->left = pipelineTree(tok) ;
		pipeline->right = stageTree(tok) ;
		// check
		if(pipeline->right == NULL || pipeline->left == NULL)
			return NULL ;
		return pipeline ;
	}//end if <pipeline>|<stage> case
	// case where <pipeline> = <stage>
	return stage ;
}//end pipelineTree

// returns <and-or> tree
CMD *andorTree(token *tok){
	// get <pipeline> and check for success
	CMD *pipeline = stageTree(tok) ;
	if(pipeline == NULL)
		return NULL ;
	// case where <and-or>...<pipeline>
	if(tok->type == SEP_AND || tok->type == SEP_OR){
		CMD *andor = mallocCMD() ;
		andor->type = tok->type ;
		tok = tok->next ;
		andor->left = andorTree(tok) ;
		andor->right = pipeline ;
		// check
		if(andor->left == NULL || andor->right == NULL)
			return NULL ;
		return andor ;
	}//end if <and-or>...<pipeline> case
	// case where <and-or> = <pipeline>
	return pipeline ;
}//end andorTree


// returns <sequence> tree
CMD *sequenceTree(token *tok){
	// get <and-or>
	CMD *andor = andorTree(tok) ;
	if(andor == NULL)
		return NULL ;
	// case where <sequence>...<and-or>
	if(tok->type == SEP_END || tok->type == SEP_BG){
		CMD *sequence = mallocCMD() ;
		sequence->type = tok->type ;
		tok = tok->next ;
		sequence->left = andor ;
		sequence->right = sequenceTree(tok) ;
		// check 
		if(sequence->left == NULL || sequence->left == NULL)
			return NULL ;
		return sequence ;
	}// end if <sequence>...<and-or>
	// case where <sequence> = <and_or>
	return andor ;
}//end sequenceTree

// returns <command> tree
CMD *commandTree(token *tok){
	// get <sequence>
	CMD *sequence = sequenceTree(tok) ;
	if(sequence == NULL)
		return NULL ;
	// case where <sequence> ; or <sequence> &
	if(tok->type == SEP_END || tok->type == SEP_BG){
		CMD *command = mallocCMD() ;
		command->type = tok->type ;
		tok = tok->next ;
		command->left = sequence ;
		command->right = NULL ;
		// Check
		if(command->left == NULL || tok != NULL)
			return NULL ;
		return command ;
	}// end if <sequence> ; or <sequence> &
	return sequence ;
}//end commandTree

// todo:
// subcommandTree() helper function
// need to figure out where this is nested?? at the <stage> level?
